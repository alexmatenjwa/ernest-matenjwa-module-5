import 'package:em_module5/screens/welcome.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyB6X0tvoHyW8R7SWrrF7L2QIMS04YKAh7A",
          authDomain: "flutter-todo-app-2ff85.firebaseapp.com",
          projectId: "flutter-todo-app-2ff85",
          storageBucket: "flutter-todo-app-2ff85.appspot.com",
          messagingSenderId: "833454098812",
          appId: "1:833454098812:web:a2ad4fa572c1cec47efcf9"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'My Todos',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: welcomeScreen());
  }
}
