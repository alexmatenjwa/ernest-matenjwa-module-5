import 'dart:html';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Totos extends StatefulWidget {
  const Totos({Key? key}) : super(key: key);

  @override
  State<Totos> createState() => _TotosState();
}

class _TotosState extends State<Totos> {
  final Stream<QuerySnapshot> myTodos =
      FirebaseFirestore.instance.collection("todos").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _title = TextEditingController();
    TextEditingController _desc = TextEditingController();
    TextEditingController _cat = TextEditingController();

    void deleteTodo(todoId) {
      FirebaseFirestore.instance
          .collection("todos")
          .doc(todoId)
          .delete()
          .then((value) => print("Todo succesfully deleted"));
    }

    void editTodo(data) {
      var todo = FirebaseFirestore.instance.collection("todos");

      _title.text = data["title"];
      _desc.text = data["description"];
      _cat.text = data["category"];
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("UPDATE"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: _title,
                    ),
                    TextField(
                      controller: _desc,
                    ),
                    TextField(
                      controller: _cat,
                    ),
                    TextButton(
                        onPressed: () {
                          todo.doc(data["todo_id"]).update({
                            "title": _title.text,
                            "description": _desc.text,
                            "category": _cat.text
                          });
                          Navigator.pop(context);
                        },
                        child: Text("UPDATE"))
                  ],
                ),
              ));
    }

    return StreamBuilder(
      stream: myTodos,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                height: (MediaQuery.of(context).size.height),
                width: (MediaQuery.of(context).size.width),
                child: ListView(
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot documentSnapshot) {
                    Map<String, dynamic> data =
                        documentSnapshot.data()! as Map<String, dynamic>;
                    return Column(
                      children: [
                        Card(
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(data['title']),
                                subtitle: Text(data['description'] +
                                    ' | ' +
                                    data['category']),
                              ),
                              ButtonTheme(
                                  child: ButtonBar(
                                children: [
                                  OutlinedButton.icon(
                                    onPressed: () {
                                      editTodo(data);
                                    },
                                    icon: Icon(Icons.edit),
                                    label: Text("Edit"),
                                  ),
                                  OutlinedButton.icon(
                                    onPressed: () {
                                      deleteTodo(data["todo_id"]);
                                    },
                                    icon: Icon(Icons.delete),
                                    label: Text("Delete"),
                                  ),
                                ],
                              ))
                            ],
                          ),
                        )
                      ],
                    );
                  }).toList(),
                ),
              ))
            ],
          );
        } else {
          return (Text("No data to show"));
        }
      },
    );
  }
}
