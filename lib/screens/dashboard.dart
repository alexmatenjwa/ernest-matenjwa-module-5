import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:em_module5/screens/todos.dart';

class dashboadScreen extends StatelessWidget {
  const dashboadScreen({Key? key}) : super(key: key);

  // final Stream<QuerySnapshot> myTodos =
  //     FirebaseFirestore.instance.collection("todos").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController titleController = TextEditingController();
    TextEditingController discController = TextEditingController();
    TextEditingController catController = TextEditingController();

    Future addTodo() {
      final title = titleController.text;
      final desc = discController.text;
      final cat = catController.text;

      final ref = FirebaseFirestore.instance.collection("todos").doc();

      return ref
          .set({
            "title": title,
            "description": desc,
            "category": cat,
            "todo_id": ref.id
          })
          .then((value) => {
                titleController.text = "",
                discController.text = "",
                catController.text = "",
              })
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Create a to do list'),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: titleController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: 'Enter todo title'),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: discController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: 'Enter todo Discription'),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: catController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: 'Enter todo category'),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                addTodo();
              },
              child: Text('Add todo')),
          Totos()
        ],
      ),
    );
  }
}
